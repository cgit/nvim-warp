" Uses a strategy that allows the user to select an arbitrary line and column by
" using 2x2 key strokes. Is more complex, but most powerful.
command! WarpFull
      \ lua require('warp').run(
      \   require('warp.win_selectors.prompt'),
      \   require('warp.row_selectors.grid').with_opts({ two_columns = true }),
      \   require('warp.col_selectors.grid'))

" Uses a strategy that allows the user to select an arbitrary line and column by
" using 2x2 key strokes. Is more complex, but most powerful.
command! WarpGrid
      \ lua require('warp').run(
      \   require('warp.win_selectors.null'),
      \   require('warp.row_selectors.grid').with_opts({ two_columns = true }),
      \   require('warp.col_selectors.grid'))

" Has the user select the column based on nows.
command! WarpWords lua require('warp').run(
      \   require('warp.win_selectors.null'),
      \   require('warp.row_selectors.grid').with_opts({ two_columns = true }),
      \   require('warp.col_selectors.words'))

" Warps to the line and leaves the cursor on the current line.
command! WarpLine lua require('warp').run(
      \   require('warp.win_selectors.null'),
      \   require('warp.row_selectors.grid').with_opts({ two_columns = true }),
      \   require('warp.col_selectors.null'))

" Warps to the column and leaves the cursor on the same line
command! WarpCol lua require('warp').run(
      \   require('warp.win_selectors.null'),
      \   require('warp.row_selectors.null'),
      \   require('warp.col_selectors.grid'))

" Warps to a window
command! WarpWin
      \ lua require('warp').run(
      \   require('warp.win_selectors.prompt'),
      \   require('warp.row_selectors.null'),
      \   require('warp.col_selectors.null'))


noremap <Plug>(warp-grid) <cmd>WarpGrid<cr>
noremap <Plug>(warp-words) <cmd>WarpWords<cr>
noremap <Plug>(warp-line) <cmd>WarpLine<cr>

onoremap v<Plug>(warp-grid) <cmd>WarpGrid<cr>
onoremap v<Plug>(warp-words) <cmd>WarpWords<cr>
onoremap V<Plug>(warp-line) <cmd>WarpLine<cr>
