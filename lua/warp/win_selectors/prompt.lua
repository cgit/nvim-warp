vim = assert(vim)
local M = {}

local function open_panel_in_rect(c, row, col, w, h)
  local buf = vim.api.nvim_create_buf(0, 1)
  local win = vim.api.nvim_open_win(buf, false, {
    relative = 'editor',
    row = row + math.floor(h / 2) - 2,
    col = col + math.floor(w / 2) - 5,
    width = 11,
    height = 5,
    focusable = true,
    style = 'minimal'
  })
  vim.api.nvim_win_set_option(win, "winhighlight", "Normal:WarpNormal")
  vim.api.nvim_buf_set_lines(buf, 0, -1, 0, { '', '', '     ' .. c, '', '' })
  return win
end

local function next_char()
  vim.cmd("redraw!")
  return vim.fn.nr2char(vim.fn.getchar())
end

M.select_win = function ()
  local wins = vim.api.nvim_list_wins()

  local alphabet = "abcdefghijklmnopqrstuvwxyz"
  local ai = 1
  local char_to_win = {}
  local to_clean = {}

  for _, w in pairs(wins) do
    local rowcol = vim.api.nvim_win_get_position(w)
    local width = vim.api.nvim_win_get_width(w)
    local height = vim.api.nvim_win_get_height(w)

    local char = alphabet:sub(ai, ai)
    table.insert(to_clean, open_panel_in_rect(char, rowcol[1], rowcol[2], width, height))
    ai = ai + 1
    char_to_win[char] = w
  end

  local sel = next_char()

  for _, w in pairs(to_clean) do
    vim.api.nvim_buf_delete(vim.api.nvim_win_get_buf(w), {force = true})
  end

  if sel == '.' then
    return 0
  else
    return char_to_win[sel]
  end
end


return M
