local vim = assert(vim)

local M = {}
local alphabet = "etansihrdlocumwfgypkbvjxqz"
local util = require('warp.util')

local function split_lines(line)
  local i
  local j = 1

  local chars_to_col = {}
  local col_to_chars = {}

  local run_regex = function(rx, off)
    if #alphabet < j then return end

    if not off then off = 0 end

    i = 0
    while true do
      i = string.find(line, rx, i)
      if i == nil then break end
      i = i + 1

      if #alphabet < j then return end

      local nextalph = alphabet:sub(j, j)
      j = j + 1
      chars_to_col[nextalph] = i + off
      col_to_chars[i + off] = nextalph
    end
  end

  run_regex("%W%w")
  run_regex("%s%S")
  run_regex("%w%W", -1)
  run_regex("[a-z][A-Z]")

  local winstr = ""
  i = 1
  while i <= #line do
    if col_to_chars[i] then
      winstr = winstr .. col_to_chars[i]
    else
      winstr = winstr .. " "
    end
    i = i + 1
  end

  return winstr, chars_to_col
end

M.run = function ()
  M.words_strategy().run()
end

M.words_strategy = function()
  local chartab

  return util.wrap_col_selector({
    display = function()
      local curpos = vim.api.nvim_win_get_cursor(0)
      local line_at = vim.fn.getline(curpos[1])
      local str, tab = split_lines(line_at)
      chartab = tab
      return str
    end,

    on_char = function(ch)
      if ch == '$' or ch == '^' then
        vim.cmd("normal! " .. ch)
      elseif not ch:match('[a-z]') then
        vim.cmd("normal! f" .. ch)
      else
        if chartab[ch] then vim.cmd("norma! " .. chartab[ch] .. "|") end
      end
      return false
    end
  })
end

return M
