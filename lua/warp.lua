local vim = assert(vim)
local M = {}

M.run = function(win_selector, row_selector, col_selector)
  if not row_selector then
    row_selector = require('warp.row_selectors.grid')
  end

  if not col_selector then
    col_selector = require('warp.col_selectors.grid')
  end

  if not win_selector then
    win_selector = require('warp.win_selectors.null')
  end

  local current_pos = vim.api.nvim_win_get_cursor(0)
  local old_scroll = vim.o.scrolloff
  vim.o.scrolloff = 0

  local f = function()
    local jump_to_win = win_selector.select_win()
    if not jump_to_win then
      return
    end
    vim.cmd([[redraw!]])
    if jump_to_win ~= 0 then
      vim.api.nvim_set_current_win(jump_to_win)
    end

    local r = row_selector.read_row()
    if not r then return end
    if r == '.' then
      r = current_pos[1]
    end

    vim.cmd("normal! m'")
    vim.api.nvim_win_set_cursor(0, {r, current_pos[2]})

    col_selector.run()
  end
  f ()

  vim.o.scrolloff = old_scroll
end

return M
